from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from database_setup import Base, Category, Item, User

engine = create_engine('sqlite:///catalogitems.db')
Base.metadata.bind = engine

DBSession = sessionmaker(bind=engine)
session = DBSession()



current_user = User(name="Test User", email="testUser@example.com")
session.add(current_user)
session.commit()


categories = [
    ['soccer',
        [{'name': 'two shinguards',
          'description': 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Accusantium, voluptatum, perspiciatis aliquid quia distinctio ea est fuga aspernatur sapiente ad nobis. Laborum aliquam quae perspiciatis. Eius tenetur ipsa quaerat velit.'},
         {'name': 'shinguards',
          'description': 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Accusantium, voluptatum, perspiciatis aliquid quia distinctio ea est fuga aspernatur sapiente ad nobis. Laborum aliquam quae perspiciatis. Eius tenetur ipsa quaerat velit.'},
         {'name': 'jersey',
          'description': 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Accusantium, voluptatum, perspiciatis aliquid quia distinctio ea est fuga aspernatur sapiente ad nobis. Laborum aliquam quae perspiciatis. Eius tenetur ipsa quaerat velit.'},
         {'name': 'soccer cleats',
          'description': 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Accusantium, voluptatum, perspiciatis aliquid quia distinctio ea est fuga aspernatur sapiente ad nobis. Laborum aliquam quae perspiciatis. Eius tenetur ipsa quaerat velit.'}]
    ],

    ['hockey',
        [{'name': 'stick',
          'description': 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Accusantium, voluptatum, perspiciatis aliquid quia distinctio ea est fuga aspernatur sapiente ad nobis. Laborum aliquam quae perspiciatis. Eius tenetur ipsa quaerat velit.'}]
    ],

    ['snowboarding',
        [{'name': 'goggles',
          'description': 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Accusantium, voluptatum, perspiciatis aliquid quia distinctio ea est fuga aspernatur sapiente ad nobis. Laborum aliquam quae perspiciatis. Eius tenetur ipsa quaerat velit.'},
         {'name': 'snowboard',
          'description': 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Accusantium, voluptatum, perspiciatis aliquid quia distinctio ea est fuga aspernatur sapiente ad nobis. Laborum aliquam quae perspiciatis. Eius tenetur ipsa quaerat velit.'}]
    ],

    ['frisbee',
        [{'name': 'frisbee',
          'description': 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Accusantium, voluptatum, perspiciatis aliquid quia distinctio ea est fuga aspernatur sapiente ad nobis. Laborum aliquam quae perspiciatis. Eius tenetur ipsa quaerat velit.'}]
    ],
    ['baseball',
        [{'name': 'bat',
          'description': 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Accusantium, voluptatum, perspiciatis aliquid quia distinctio ea est fuga aspernatur sapiente ad nobis. Laborum aliquam quae perspiciatis. Eius tenetur ipsa quaerat velit.'}]
    ]
]

for category in categories:
    current_category = Category(name=category[0], user=current_user)
    session.add(current_category)
    session.commit()


    for item in category[1]:
        current_item = Item(name=item['name'],
                            description=item['description'],
                            category=current_category,
                            user=current_user)
        session.add(current_item)
        session.commit()
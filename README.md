# Catalog

# Description

This is a basic catalog application using CRUD functionality and 3rd party authentication and authorization integrations, this application also uses API endpoints, the response format can be either JSON. This application is built with python and flask.

## Requirements
* Python 2.7.11
* SQLite 3.9.2
* Flask 0.9
* SQLAlchemy 1.0.12
* Google+ Client Secrets(client_secrets.json)

## Usage
* To create database:
```
  python database_setup.py
```
* To populate database with data:
```
  python fakecontent.py
```
* Run the application:
```
  python application.py
```
* Navigate to http://localhost:5000


## JSON endpoint 
```
  /catalogs/JSON
```
```
  /catalogs/<category_name>/items/JSON
```
```
 /catalogs/<category_name>/items/<item_name>/JSON
```


